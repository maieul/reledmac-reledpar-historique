\documentclass[transnotheorems,noamsthm]{beamer}
\usepackage{fontspec,polyglossia,xunicode,hyperref,csquotes}
\usepackage{graphicx}
\usepackage{metalogo}
\graphicspath{{img/}{}}
\usetheme[]{Boadilla}
\newenvironment{slide}{%
  \begin{frame}
  <presentation>\mode<presentation>{\frametitle{\insertsubsection}}%
  }%
  {\end{frame}}
\newenvironment{slide*}{\begin{frame}<presentation>[<1>]\mode<presentation>{\frametitle{\insertsubsection}}}{\end{frame}}


\beamerdefaultoverlayspecification{<+->}
\usepackage{xparse}
\usepackage{libertineotf}
\setmainlanguage{french}
\setotherlanguage{english}
\usepackage{minted}
\newcounter{code}
\resetcounteronoverlays{code}
\usepackage{hyperref}
\hypersetup{bookmarksdepth=6}
\renewcommand{\thecode}{\arabic{code}}
\newenvironment{attention}%
  {\begin{alertblock}{Attention}}%
  {\end{alertblock}}
\newenvironment{plusloin}%
  {\begin{block}{Pour aller plus loin}}%
  {\end{block}}
\NewDocumentCommand{\code}{mmo}{%
  \stepcounter{code}%
  \begin{block}{code \thecode: #2}%
			\begin{english}\footnotesize\IfNoValueF{#3}{#3}%
        \inputminted[linenos=true,breaklines=true]{latex}{code/#1}%
			\end{english}%
  \end{block}
	}

\usepackage[noend,noeledsec,nofamiliar,series={A}]{reledmac}
\firstlinenum{1}
\linenumincrement{1}
\Xarrangement{paragraph}

\newcommand{\alertdesc}[2]{\item[\alert{#1}]#2}
\newcommand{\meta}[1]{\texttt{<#1>}}
\newcommand{\extension}[1]{\texorpdfstring{\alert{.#1}}{.#1}}
\newcommand{\package}[1]{\alert{#1}}
\newcommand{\conclusion}{\alert{$\Rightarrow$}}
\newcommand{\cs}[1]{\texttt{\textbackslash#1}}
\newcommand{\opt}[1]{\textbf{\texttt{#1}}}
\setcounter{tocdepth}{1}
\AtBeginSection{
	\mode<presentation>{\frame{\sectionpage}}
}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}%remove navigation symbols


    
\author{Maïeul Rouquette}
\title{De \package{edmac} à \package{reledmac} et \package{reledpar}}
\subtitle{Historique et perspectives pour les éditions critiques avec \LaTeX}
\institute{Université de Lausanne --- IRSB}
\date{11-12 septembre 2017 --- Journées \LaTeX\ Enssib}

\begin{document}


\begin{frame}
	\titlepage
	\vfill
	{\tiny Licence Creative Commons France 3.0 - Paternité - Partage à l'identique
	
	\url{http://geekographie.maieul.net/214}
	}
\end{frame}

\begin{frame}
\tableofcontents
\end{frame}

\section{Édition critique : quesaco ?}
\subsection{Un texte nous parvient}
\begin{slide}
  \begin{itemize}
    \item Soit par un témoin posant des difficultés de lecture:
      \begin{itemize}
        \item Problèmes de conservation matérielle 
        \item Problèmes paléographiques
        \item Problèmes de corrections éventuelles (ex.
 brouillon d'écrivain)
      \end{itemize}
    \item Soit par plusieurs témoins possédant des divergences:
      \begin{itemize}
        \item Changements (volontaires ou non) lors de la copie des œuvres antiques et médiévales
        \item Présence de plusieurs brouillons d'un même œuvre
        \item Pour les œuvres post imprimerie, divergences dans les différents imprimés (ex. roman dans journal puis sous forme de livre)
      \end{itemize}
  \end{itemize}

\end{slide}
\subsection{Role de l'éditeur·trice \emph{scientifique}}
\begin{slide}
  \begin{itemize}
    \item Analyse les témoins du texte
    \item Les compare et détermine leurs relations
    \item Établit une édition du texte:
      \begin{itemize}
        \item Soit à partir d'un témoin de base éventuellement corrigé
        \item Soit à partir de plusieurs témoins (édition \enquote{ecléctique})
      \end{itemize}
    \item En procédant éventuellement à des normalisations (orthographiques principalement) 
  \end{itemize}
\end{slide}
\subsection{Autour du texte édité}
\begin{slide}
 \begin{itemize}
   \item Une introduction que justifie les choix de l'éditeur·trice
   \item Un \emph{apparat textuel} indiquant:
     \begin{itemize}
       \item Les corrections ou suggestions de correction de l'éditeur·trice
       \item Les principales variantes textuelles
       \item Les problèmes de lecture des témoins
       \item Toutes les remarques jugées utiles sur la transmission d'un passage précis (ex. correction dans les manuscrit)
     \end{itemize}
 \end{itemize}
\end{slide}
\subsection{Compléments fréquents à l'édition critique}
\begin{slide}
  \begin{itemize}
    \item Un \emph{apparat des sources}
    \item Des notes de commentaire historiques ou philologiques
    \item Une traduction en langue moderne
    \item Un ou des index
    \item Un commentaire
    \item Une bibliographie
  \end{itemize}
\end{slide}
\subsection{Conséquences typographiques}
\begin{slide}
  \begin{itemize}
    \item Besoin de plusieurs niveaux de notes de bas de page / de fin
    \item Besoin de notes qui renvoient à un (ou plusieurs) mot(s) d'une page, à une ligne précise
    \item Besoin le cas échéant d'avoir une traduction en parallèle
  \end{itemize}
\end{slide}
\subsection{Exemple (fictif)}
\begin{slide}
  \hfill\begin{minipage}{0.8\textwidth}
    \beginnumbering
      \pstart
    Le petit \edtext{chat}{\Afootnote{A : chien}} est \edtext{mort}{\Afootnote{B : décédé}}.
 Il est tombé du toit.
 Pourquoi est-ce \edtext{toujours}{\Afootnote{C : \emph{om.}}} les petits chats qui meurent et jamais les papes qui tombent du \edtext{toit}{\Afootnote{AD: \emph{add.} par terre}} ?
     \pend
    \endnumbering
  \end{minipage}
  \hfill\null
\end{slide}

\section{De \package{edmac} à \package{ledmac} et \package{ledpar}}
\subsection{\package{edmac}}
\begin{slide}
  \begin{itemize}
    \item Pour Plain\TeX
    \item John Lavignino et Dominik Wujastyk (1988-1989)
    \item Pose les bases:
        \begin{itemize}
          \item Numérotation des lignes (configurable)
          \item Notes critiques sur plusieurs niveaux
        \end{itemize}
      \item En 1994, treize ouvrages publiés avec \package{edmac}
  \end{itemize}
\end{slide}
\begin{slide}
\begin{overprint}
  \onslide<1->{\code{mwe_edmac.tex}{Exemple minimum avec \package{edmac}}}
  \onslide<2->{\conclusion\  Toute la base y est}

  
  \onslide<3->{\conclusion\ Complété ensuite par \package{edstanza} (poésie) et \package{edtab} (tableaux), toujours pour Plain\TeX}
\end{overprint}
\end{slide}
\subsection{\package{ledmac}}
\begin{slide}
  \begin{itemize}
    \item 2003 : Peter Wilson porte pour \LaTeX\  \package{edmac} et ses compléments \conclusion\ \package{ledmac}
    \item \enquote{\textenglish{A presumptuous attempt to port EDMAC, TABMAC and EDSTANZA to LaTeX}}
    \item À titre personnel, je n'ai jamais compris ce sous-titre
  \end{itemize}
\end{slide}
\begin{slide}
    \begin{itemize}
    		\item Se charge comme un package avec \cs{usepackage}
        \item Emploi d'une syntaxe \LaTeX\  pour les commandes (délimitation des arguments)
        \item Résolution conflit de noms (\cs{label}$\rightarrow$\cs{edlabel})
        \item Indexation
        \item Notes de bas page \enquote{familières} (avec appel de note) sur plusieurs niveaux
    \end{itemize}
\end{slide}
\begin{slide}
  
  \code{mwe_ledmac.tex}{Exemple minimum avec \package{ledmac}}
\end{slide}
\subsection{\package{ledpar}}
\begin{slide}
  \begin{itemize}
    \item 2004 : Peter Wilson crée \package{ledpar}
    \item Permet de mettre deux textes critiques en parallèle:
      \begin{itemize}
        \item Soit deux colonnes en parallèle
        \item Soit deux pages en parallèle      
      \end{itemize}
    \item Cas le plus fréquent : texte et traduction
  \end{itemize}
    
\end{slide}

\begin{slide}
\code{reledpar-pages.tex}{Exemple d'emploi de \package{ledpar}}[\tiny]
\end{slide}
\subsection{\package{ledarab}}

\begin{slide}
  \begin{itemize}
    \item 2003 : Peter Wilson crée \package{ledarab} 
    \item Se base sur \package{arabtex} mais compatible avec les fonctionnalités de \package{ledmac}
  \end{itemize}
\end{slide}

\section{De \package{ledmac} et \package{ledpar} à \package{reledmac} et \package{reledpar}}
\subsection{Comment je suis arrivé à reprendre \package{ledmac} et \package{ledarab}}
\begin{slide}
  \begin{itemize}
    \item 2003-2005: Peter Wilson maintient \package{ledmac} / \package{ledpar}
    \item Sur internet, on trouve différentes astuces pour adapter \package{ledmac} / \package{ledpar} à ses besoins
    \item En 2011, une connaissance me parle de l'une d'entre elle 
    \item Je l'intègre à \package{ledpar}
    \item Je propose sur le CTAN
    \item[\conclusion] Occasion de découvrir la LPPL et le système de mainteneur
  \end{itemize}
\end{slide}
\subsection{Pris au piège de la maintenance bénévole}
\begin{slide}
  \begin{enumerate}
    \item J'ouvre un dépôt Github
    \item Pas mal de demande arrivent
    \item Notamment des demandes pour personnaliser plus facilement l'affichage des notes
  \end{enumerate}
\end{slide}
\subsection{\package{eledmac} et \package{eledpar}}
\begin{slide}
  \begin{itemize}
    \item Constats fait en 2012:
      \begin{itemize}
        \item Du code redondant dans \package{ledmac} (niveaux de note)
        \item Des astuces pour personnaliser l'affichage:
          \begin{itemize}
            \item Complexes à comprendre pour les utilisateur·trice·s
            \item Souvent incompatibles entre elles
          \end{itemize}
      \end{itemize}
    \item[\conclusion] Ajouter dans les commandes existantes des points d'entrée, que j'ai appelés \enquote{hooks}
    \item[\conclusion] Les hooks peuvent être facilement configurés dans le préambule
  \end{itemize}
\end{slide}

\begin{slide}
  \code{hooks_eledmac.tex}{Exemple de hooks avec \package{eledmac}}
  
    \Xnumberonlyfirstinline
    \Xsymlinenum{$||$}
    \hfill\begin{minipage}{0.8\textwidth}
    \beginnumbering
      \pstart
    Le petit \edtext{chat}{\Afootnote{A : chien}} est \edtext{mort}{\Afootnote{B : décédé}}.
 Il est tombé du toit.
 Pourquoi est-ce \edtext{toujours}{\Afootnote{C : \emph{om.}}} les petits chats qui meurent et jamais les papes qui tombent du \edtext{toit}{\Afootnote{AD: \emph{add.} par terre}} ?
     \pend
    \endnumbering
  \end{minipage}
  \hfill\null
\end{slide}
\begin{slide}
  \begin{itemize}
    \item Les utilisateurs n'ont plus à surcharger les commandes de bas niveau (\enquote{internes})
    \item Mais j'ai du ajouter des arguments pour ces commandes afin de pouvoir \alert{régler les hooks niveau de note par niveau de note}
    \item[\conclusion] Rupture de compatibilité pour les gens qui modifiaient ces commnandes de bas niveau
    \item[\conclusion] Nécessité de changer de nom pour éviter les surprises lors des mises à jour
  \end{itemize}
\end{slide}
\begin{slide}
  \begin{itemize}
    \item \package{eledmac} et \package{eledpar}
    \item \enquote{e} pour \enquote{extended}\ldots
    \item \ldots\ mais aussi pour \package{etoolbox}
  \end{itemize}
\end{slide}

\begin{slide}
  \begin{itemize}
    \item Accessoirement, abandon de \package{ledarab}
  \item L'écriture de droite à gauche doit être gérée par les moteurs modernes : \XeTeX\ ou \LuaTeX
  \end{itemize}
\end{slide}
\subsection{\package{reledmac} et \package{reledpar}}

\begin{slide}
  \begin{itemize}
    \item De plus en plus de hooks, aux noms parfois incohérents 
    \item Des réglages parfois par appel de commande, parfois par redéfinition de commande (\cs{renewcommand})
    \item Des commandes définies dans \package{eledmac} et redéfinies dans \package{eledpar} (\cs{renewcommand}) \conclusion\ augmente le risque d'erreur
    \item La routine de sortie (\cs{output})  surchargée et non pas patchée \conclusion Risque accrue d'incompatibilité
  \end{itemize}
\end{slide}

\begin{slide}
  \begin{itemize}
    \item Relecture de tout le code
    \item Utilisation des commandes de patchage de \package{etoolbox}
    \item Définition d'une convention de nommage pour les hooks
      \begin{description}
        \item[Nom commençant par X] applicable aux notes critiques
        \item[Nom finissant par X] applicable aux notes familières (classiques)
        \item[Nom commençant par Xend] applicable aux notes de fin (notes critiques)
      \end{description}
    \item \package{reledmac} et \package{reledpar} : \enquote{r} comme \enquote{\textenglish{renewed}}
  \end{itemize}
\end{slide}
\section{Bilan de six ans de travail (3 septembre 2017)}
\subsection{Statistiques}
\begin{slide}
  \begin{itemize}
    \item Près de six cent améliorations / bugs résolus
    \item Plus de deux cent versions sorties
    \item Plus de huit mille commits
  \end{itemize}
\end{slide}
\subsection{Principales améliorations}
\begin{slide}
  \begin{itemize}
    \item Réorganisation de la documentation
    \begin{itemize}
    	\item Séparation claire 
        \begin{itemize}
          \item Documentation de l'interface utilisateur·trice
          \item Documentation du code
          \item Exemples
        \end{itemize}
      \item Ajout d'intertitres et emploi d'\package{hyperref}
     \end{itemize}
    \item Messages d'erreur plus explicites en cas de faute de syntaxe
  \end{itemize}
\end{slide}
\begin{slide}
  \begin{itemize}
    \item Cent-trente-sept hooks
    \item Gestion des intertitres (\cs{section} etc.)
    \item Meilleure cohésion \package{reledmac} / \package{reledpar}
    \item Nouvelles options de synchronisation pour \package{reledpar}
    \item Gestion des lemmes ambigus (si plusieurs fois le même mot sur une même ligne)
    \item Options pour ne pas charger certaines fonctions si non nécessaires
    \item Compatibilité accrue avec certains packages
    \item Liste non exhaustive\ldots
  \end{itemize}
\end{slide}
\section{Limites et souhaits}
\subsection{Limites personnelles}

\begin{slide}
  \begin{itemize}
    \item Limites de temps (travail uniquement bénévole)
    \item L'anglais n'est pas ma langue maternelle
    \item Non connaissance des subtilités des moteurs modernes pour la gestion de l'écriture de droite à gauche
  \end{itemize}
\end{slide}

\subsection{Complexités inhérentes au fonctionnement de \package{reledmac}/\package{reledpar}}

\begin{slide}
  \begin{itemize}
    \item Découpage des paragraphes en lignes via \cs{vsplit} \conclusion\ problèmes avec le texte \enquote{hors paragraphe classique}  (titres, listes, images, etc.)
    \item Décalage entre le moment où le texte est lu par \TeX\ et le moment où il est formaté (surtout \package{reledpar}) \conclusion Vigilance constante!
  \end{itemize}
\end{slide}

\subsection{Dette technique avec \package{reledpar}}
\begin{slide}

  \begin{itemize}
    \item Dissymétrie entre les commandes internes pour le texte de gauche et celle pour le texte de droite:
      \begin{itemize}
        \item Certaines commandes  pour le texte de gauche s'appliquent aussi lorsqu'on est en mode \enquote{colonne unique}
        \item Les commandes  pour le texte de droite ne s'appliquent qu'au texte de droite
      \end{itemize}
    \item Duplication du code entre commandes internes de droite et celles de gauche
  \item Conception binaire de la mise en parallèle de textes \conclusion\ Impossible d'envisager la mise en parallèle de trois textes sans réécriture d'env.
     90~\% du code
  \end{itemize}
\end{slide}
\subsection{Souhaits}
\begin{slide}
  \begin{itemize}
    \item Continuer à faire les améliorations \enquote{simples} aussi régulièrement que possible
    \item Concevoir des schémas pour expliquer le   fonctionnement interne des packages
    \item Créer d'avantage d'ECM pour s'assurer d'éviter les ruptures de compatibilité involontaires
    \item Impliquer d'avantage les utilisateur·trice·s dans le projet:
      \begin{itemize}
        \item Documentation
        \item Exemples
        \item Problèmes simples à résoudre
      \end{itemize}
    \item Trouver un financement d'un an après mon contrat actuel pour pouvoir améliorer la mise en parallèle
  \end{itemize}
\end{slide}


\begin{frame}
\null\hfill
Je vous remercie
\hfill\null
\end{frame}


\end{document}
